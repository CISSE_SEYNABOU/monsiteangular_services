import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenegalHistoireComponent } from './senegal-histoire.component';

describe('SenegalHistoireComponent', () => {
  let component: SenegalHistoireComponent;
  let fixture: ComponentFixture<SenegalHistoireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenegalHistoireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenegalHistoireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
