import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root',
})
export class ExportRegionsService {
  getRegions() : {id:number,nom:string}[]{
    let regions=[
      {id:1, nom:'Dakar' },
      {id:2, nom:'Thies' },
      {id:3, nom:'Diourbel' },
      {id:4, nom:'Kaolack' },
      {id:5, nom:'Louga' },
      {id:6, nom:'Fatick' },
      {id:7, nom:'Tambacounda' },
      {id:8, nom:'Ziguinchor' },
      {id:9, nom:'Saint-Louis' },
      {id:10, nom:'Podor' },
      {id:11, nom:'Matam' },
      {id:12, nom:'Kolda' },
      {id:13, nom:'Seddhiou' },
      {id:14, nom:'Kaffrine' } 
    ];
    return regions;
  }

  constructor() { }
}
