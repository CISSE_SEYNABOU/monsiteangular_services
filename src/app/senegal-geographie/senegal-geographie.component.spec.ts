import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenegalGeographieComponent } from './senegal-geographie.component';

describe('SenegalGeographieComponent', () => {
  let component: SenegalGeographieComponent;
  let fixture: ComponentFixture<SenegalGeographieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenegalGeographieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenegalGeographieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
